import pika
import time
from pymongo import MongoClient
from pprint import pprint
from random import randint
import os

pathToFile = os.environ.get('IP_NODES')
NODE_IPS = [None]*11
numberOfNodes = 20

def insertIP(ip):
    for line in range(NODE_IPS):
        if ((str(None) in str(NODE_IPS[line])) == False):
            NODE_IPS[line] = ip
            
def fullfillENV():
    ipFile = open(str(pathToFile),"r")
    for line in ipFile:
        insertIP(str(line))
        

def connectMongo(ip, port):
    for x in range(0, numberOfNodes):
        client = MongoClient(ip, port)
        db=client.Database
        serverStatusResult=db.command("serverStatus")
        if '\'ismaster\': True,' in str(serverStatusResult):
            #pprint(serverStatusResult)
            return client
    return "Error"
            
def saveData(client, payload, uniqueNumber):
    randomData = {
        'payload' : payload,
        'uniqueNumber' : uniqueNumber
    }
    db=client.Database
    contacts = db.contacts
    if(contacts.find({"uniqueNumber":uniqueNumber}).count() > 0):
        print("Data: " + payload + " already in the db")
    else:
        result=contacts.insert(randomData)
        print("Data: " + payload + " saved")

def getEntry(client, uniqueNumber):
    db=client.Database
    contacts = db.contacts
    print(contacts.find({"uniqueNumber":uniqueNumber}))

def callback(ch, method, properties, body):
    print(" [x] Received %r" % body)
    splitted = str(body).split(";")
    if(len(splitted)>1):
        saveData(client, splitted[0], splitted[1])
    print(" [x] Done")
    ch.basic_ack(delivery_tag = method.delivery_tag)    
    
#Main loop
#fullfillENV()

#Connnect mongo db
client = connectMongo('mongo', 27017)
print("connected to mongodb")

#Connect mqtt and configure
credentials = pika.PlainCredentials('admin', 'password')
parameters = pika.ConnectionParameters('rabbitmq', 5672, '/', credentials)
connection = pika.BlockingConnection(parameters)
print("connected to rabbitmq")
channel = connection.channel()
channel.queue_declare(queue='task_queue', durable=True)
channel.basic_qos(prefetch_count=2)
channel.basic_consume('task_queue', callback)
channel.start_consuming()




